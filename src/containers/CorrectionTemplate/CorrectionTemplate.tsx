/**
 * File Name: OfficeHolderTemplate
 * Purpose: This file contains the React component rendered on the detailed    *    results page (Home->Search Results->Detailed Results)
 * Created: March 2021
 * Revised:
 *    > Anna Mollere (5/16/2021) - added code comments
 * References:
 *    >
 * Author(s): Anna Mollere (OSU 2020/21), Joey Didner (OSU 2020/21), Kevin Carman (OSU Summer 2021)
 **/

import React from 'react';
import axios from 'axios';
import { Formik } from 'formik';
import * as Yup from 'yup';
//import ReactTooltip from 'react-tooltip';
//import copy from 'clipboard-copy';
import { useQuery, gql } from '@apollo/client';
import { useRouter } from 'next/router';
import { date } from 'yup';
//import { Dialog, Transition } from '@headlessui/react';
import { useContext } from 'react';
import { FirebaseContext } from '../../FirebaseContext';

/** In type
 * In typescript you must define the interfaces and types of the data you that you expect to GET back from the Apolloclient Query
 * These interfaces were created from the schema that should be avaiable in the Data Service repository
 */
interface officeInventoryData {
  office: Office;
}

interface Office {
  id: string;
  title: string;
  website: string;
  state: string;
  term: DateRange;
  filingWindow: DateRange;
  officeholder: OfficeHolder;
  contact: OfficeContact;
}

interface DateRange {
  start: number;
  end: number;
}
interface OfficeHolder {
  termEnd: string;
  name: OfficeHolderName;
  socialMedia: OfficeHolderSocialMedia;
}

interface OfficeHolderName {
  first: string;
  last: string;
}

interface OfficeHolderSocialMedia {
  facebook: string;
  twitter: string;
  instagram: string;
}

interface OfficeContact {
  phone: PhoneNumber;
  email: string;
}

interface PhoneNumber {
  country: string;
  area: string;
  office: string;
  line: string;
}

interface OfficeVars {
  newOfficeId?: any; //check any type
}

/**
 * This is the query itself.
 * We are using apolloclient and many of the parts of this query were derived from the apolloclient typescript docs and the useQuery hook
 * You may find these resources
 * https://www.apollographql.com/docs/react/api/react/hooks/
 * https://www.apollographql.com/docs/react/development-testing/static-typing/
 * Check the relevant schema that can be found at the IP address queried from ("Should be located in _app.tsx in pages")
 * The schema will be used to reveal why we are querying twice (once for officeholder data and once for Meetings data)
 */

const GET_OFFICE_DATA = gql`
  query OfficeDetails($newOfficeId: ID!) {
    office(id: $newOfficeId) {
      id
      title
      website
      state
      term {
        start
        end
      }
      filingWindow {
        start
        end
      }
      officeholder {
        termEnd
        name {
          first
          last
        }
        socialMedia {
          facebook
          twitter
          instagram
        }
      }
      contact {
        phone {
          country
          area
          office
          line
        }
        email
      }
    }
  }
`;

//Utitlity function to translate epoch time data to human readable format for display

function translateEpochTime(epochTimeNum: number) {
  const inputNum = epochTimeNum;
  const secondOperation = inputNum * 1000;
  const newDate = new Date(secondOperation);
  const mm = newDate.getMonth() + 1; // getMonth() is zero-based
  const dd = newDate.getDate();

  return [
    newDate.getFullYear(),
    (mm > 9 ? '' : '0') + mm,
    (dd > 9 ? '' : '0') + dd,
  ].join('-');
}

//object to translate form names to DB names
const DBFieldTranslate = {
  email_add: 'contact.email',
  filing_end: 'filingWindow.end',
  filing_start: 'filingWindow.start',
  fname: 'officeholder.name.first',
  lname: 'officeholder.name.last',
  fbook: 'officeholder.socialMedia.facebook',
  insta: 'officeholder.socialMedia.instagram',
  twitter: 'officeholder.socialMedia.twitter',
  term_end: 'term.end',
  term_start: 'term.start',
  title: 'title',
  website: 'website',
};

function buildQuery(
  values: Record<string, any>,
  oldData: any,
  userID: any,
  geoID: any,
  t_officeId: any,
  DBTranslate: any
) {
  let t_source = '';
  const keys = Object.keys(values); //keys from passed in data from form
  const vals = Object.values(values); //values from form
  const dFields: any = []; //array for fields that were changed
  const t_changes: any = []; //array for changed data values
  const iter = keys.length; //length of passed in object

  //if the field is empty pass
  for (let i = 0; i < iter; i++) {
    if (vals[i] === '') {
      continue;
      //else if it is a data source write it in
    } else if (keys[i] === 'source') {
      t_source = vals[i];
      //esle if it is a phone number slice and add
    } else {
      if (keys[i] === 'phone_num') {
        //get data
        const key: string = keys[i];
        const value: string = vals[i];
        const oldValue: string = oldData[key];
        //add to change list
        dFields.push('contact.phone.area');
        //create change object
        const changeObjA = {
          fieldname: 'contact.phone.area',
          new: value.slice(0, 3),
          old: oldValue.slice(0, 3),
        };
        dFields.push('contact.phone.office');
        //create change object
        const changeObjO = {
          fieldname: 'contact.phone.office',
          new: value.slice(3, 6),
          old: oldValue.slice(3, 6),
        };
        dFields.push('contact.phone.line');
        //create change object
        const changeObjL = {
          fieldname: 'contact.phone.line',
          new: value.slice(6, 10),
          old: oldValue.slice(6, 10),
        };
        //push change object to array
        t_changes.push(changeObjA);
        t_changes.push(changeObjO);
        t_changes.push(changeObjL);

        //else just add it in
      } else {
        dFields.push(DBTranslate[keys[i]]);
        const key: string = keys[i];
        const value: string = vals[i];
        const oldValue: any = oldData[key];
        const changeObj = {
          fieldname: DBTranslate[key],
          new: value,
          old: oldValue,
        };
        t_changes.push(changeObj);
      }
    }
  }

  const query_data = {
    changedFields: dFields,
    changes: t_changes,
    source: t_source,
    corrector: userID,
    create_timestamp: Date.now(),
    geoid: geoID,
    docId: t_officeId,
    status: 'draft',
    type: 'office',
  };

  const correction_package = {
    correction: query_data,
  };

  return correction_package;
}
/**
 * Check the relevant schema that can be found at the IP address queried from ("Should be located in _app.tsx in pages")
 * The schema will be used to reveal why we are querying twice (once for officeholder data and once for Meetings data).
 * Since we have to query both of these and they both take in two different parameters for their queries,
 * We did the same trick as the last query page (search.tsx in pages) and we passed the Office ID and the GEOID into the url for parameters
 * Then we do a simple splice to split them up and are able to load in both using the UseQuery hook
 */

export const CorrectionTemplate: React.FC = () => {
  const router = useRouter();
  const { officeId } = router.query; //Returns the OfficeID AND the GEOID as one string.npm run devellp
  const stringToParse = officeId; //String parsing to separate Office ID and GeoID
  const geoid = stringToParse?.slice(20); //Store Geoid as such
  const newOfficeId = stringToParse?.slice(0, 20); //stor OFficeID as such
  const user = useContext(FirebaseContext);
  const userID = { email: user?.email, uid: user?.uid };

  const { loading: loading2, error: error2, data: data2 } = useQuery<
    officeInventoryData,
    OfficeVars
  >(GET_OFFICE_DATA, { variables: { newOfficeId } }); //storing the officeInventoryData... will be able to use as display!

  //const { loading: loading3, error: error3, data: data3 } = useQuery<
  //meetingInventoryData,
  //MeetingVars
  //>(GET_MEETINGS_DATA, { variables: { geoid } }); //storing the Meeting Inventory data... will be able to use as display!

  //if (loading2 || loading3) {
  if (loading2) {
    return <h1>Loading....</h1>;
  }
  if (error2) {
    return <h1> {error2.message}</h1>;
  }
  //if (error3) {
  //  return <h1> {error3.message}</h1>;
  // }

  /**
   * The time domain data is displayed in Epoch time and I wrote a function above to translate it to human readable time
   * Feel free to use this function before the return() statement to translate time to human readable format
   */

  const title = data2?.office.title;
  const fName = data2?.office.officeholder.name.first;

  //convert phone to one string

  const phoneArea = data2?.office.contact.phone.area;
  const phoneOffice = data2?.office.contact.phone.office;
  const phoneLine = data2?.office.contact.phone.line;

  const phoneArr = [];

  if (phoneArea) {
    phoneArr.push(phoneArea);
  }
  if (phoneOffice) {
    phoneArr.push(phoneOffice);
  }
  if (phoneLine) {
    phoneArr.push(phoneLine);
  }
  const phoneStr = phoneArr.join('');

  const termStart = data2?.office.term.start;
  const displayTermStart = translateEpochTime(termStart!);

  const termEnd = data2?.office.term.end;
  const displayTermEnd = translateEpochTime(termEnd!);

  const filingWindowStart = data2?.office.filingWindow.start;
  const displayFilingWindowStart = translateEpochTime(filingWindowStart!);

  const filingWindowEnd = data2?.office.filingWindow.end;
  const displayFilingWindowEnd = translateEpochTime(filingWindowEnd!);

  //store old data to compare
  const alt_query = {
    title: data2?.office.title,
    fname: data2?.office.officeholder.name.first,
    lname: data2?.office.officeholder.name.last,
    fbook: data2?.office.officeholder.socialMedia.facebook,
    insta: data2?.office.officeholder.socialMedia.instagram,
    twitter: data2?.office.officeholder.socialMedia.twitter,
    term_start: displayTermStart,
    term_end: displayTermEnd,
    filing_start: displayFilingWindowStart,
    filing_end: displayFilingWindowEnd,
    phone_num: phoneStr,
    email_add: data2?.office.contact.email,
    website: data2?.office.website,
  };

  // *** Targets to send correction data to ***
  //leave pointed at production unless otherwise needed

  //URL to send POST
  //production environment for Office Data Manager
  const targetURL = 'http://office-data-manager:8080/api/v1/corrections/create';

  //URL to send POST
  // Local development environment for Office Data manager
  //const targetURL = 'http://127.0.0.1:8080/api/v1/corrections/create';

  //URL to send POST
  // web based development environment for Office Data manager
  //const targetURL = 'https://enoht656sqqviqn.m.pipedream.net';

  //form validation, see Yup documentation.
  const validationSchema = Yup.object().shape({
    title: Yup.string().min(2, 'Too Short!').max(100, 'Too Long!'),
    fname: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!'),
    lname: Yup.string().min(2, 'Too Short!').max(17, 'Too Long!'),
    term_start: date(),
    term_end: Yup.date().min(
      Yup.ref('term_start'),
      'End Of Term Must Be After Start Of Term!'
    ),
    filing_start: date(),
    filing_end: Yup.date().min(
      Yup.ref('filing_start'),
      'Filing End Must Be After Filing Start!'
    ),
    phone_num: Yup.string()
      .min(10, 'Too Short!')
      .max(10, 'Enter only 10 digits, no spaces or country codes!'),
    fbook: Yup.string().min(10, 'Too Short!').max(150, 'Too Long!'),
    twitter: Yup.string().min(10, 'Too Short!').max(150, 'Too Long!'),
    insta: Yup.string().min(10, 'Too Short!').max(150, 'Too Long!'),
    website: Yup.string().min(10, 'Too Short!').max(150, 'Too Long!'),
    email_add: Yup.string()
      .email('Invalid Email.')
      .min(10, 'Too Short!')
      .max(50, 'Too Long!'),
    source: Yup.string()
      .min(10, 'Too Short!')
      .max(150, 'Too Long!')
      .required('Field Required'),
  });

  /**
   * In this section we read in and display the data using ".map" command.
   */
  return (
    <>
      <>
        {/* Office Holder Details Container */}(
        <Formik
          initialValues={{
            title: '',
            fname: '',
            lname: '',
            term_start: '',
            term_end: '',
            filing_start: '',
            filing_end: '',
            phone_num: '',
            email_add: '',
            fbook: '',
            insta: '',
            twitter: '',
            website: '',
            source: '',
          }}
          validationSchema={validationSchema}
          onSubmit={async (values, { setSubmitting, resetForm }) => {
            setSubmitting(true);
            setTimeout(() => {
              const updateQuery = buildQuery(
                values,
                alt_query,
                userID,
                geoid,
                newOfficeId,
                DBFieldTranslate
              );
              console.log(JSON.stringify(updateQuery, null, 2));
              axios
                .post(targetURL, JSON.stringify(updateQuery, null, 2), {
                  headers: { 'Content-Type': 'application/json' },
                })
                .then(function (response) {
                  console.log(response);
                })
                .catch(function (error) {
                  console.log(error);
                });
              resetForm();
              setSubmitting(false);
              window.location.href = '/';
            }, 500);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <div className="bg-white w-full transform -translate-x-0 left-1/2 h-5/6 p-5 mt-20 text-left sm:w-4/5 md:w-2/3 lg:w-1/2 m-auto">
                {/* Beginning of Description List; for office holder header */}
                <dl className="sm:divide-y sm:divide-gray-200">
                  {/* Office Holder Details Header; contains name, title, and social media links */}
                  <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                    <dt>
                      {/* Office Holder Title */}
                      <h3 className="text-xl leading-6 font-light text-gray-900">
                        Office Title
                      </h3>
                      <input
                        className="ol-span-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 focus:outline-none focus:bg-white focus:border-gray-500"
                        type="text"
                        name="title"
                        id="title"
                        placeholder={title}
                        onChange={handleChange}
                        value={values.title}
                        onBlur={handleBlur}
                      />
                      {errors.title && touched.title ? (
                        <div>{errors.title}</div>
                      ) : null}
                      {/* Office Holder Full Name */}
                      <h3 className="text-xl leading-6 font-light text-gray-900">
                        First Name
                      </h3>
                      <input
                        className={
                          (touched.fname && errors.fname ? 'has-error ' : '') +
                          'appearance-none block bg-gray-200 text-gray-700 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
                        }
                        type="text"
                        name="fname"
                        id="fname"
                        placeholder={fName}
                        onChange={handleChange}
                        value={values.fname}
                        onBlur={handleBlur}
                      />
                      {errors.fname && touched.fname ? (
                        <div>{errors.fname}</div>
                      ) : null}
                      <h3 className="text-xl leading-6 font-light text-gray-900">
                        Last Name
                      </h3>
                      <input
                        className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="lname"
                        placeholder={data2?.office.officeholder.name.last}
                        name="lname"
                        onChange={handleChange}
                        value={values.lname}
                        onBlur={handleBlur}
                      />
                      {errors.lname && touched.lname ? (
                        <div>{errors.lname}</div>
                      ) : null}
                    </dt>
                  </div>
                </dl>
                <div className="border-t border-gray-200 px-4 py-5 sm:p-0">
                  {/* Beginning of Description List; for office holder information (includes term and filing windows, contact info, and meeting info) */}
                  <dl className="sm:divide-y sm:divide-gray-200">
                    {/* Term Window */}
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt className="text-base font-medium text-gray-500">
                        Term Window
                      </dt>
                      <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="date"
                          id="term_start"
                          name="term_start"
                          placeholder={alt_query.term_start}
                          onChange={handleChange}
                          value={values.term_start}
                          onBlur={handleBlur}
                        />{' '}
                        {errors.term_start && touched.term_start ? (
                          <div>{errors.term_start}</div>
                        ) : null}{' '}
                        to
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="date"
                          id="term_end"
                          name="term_end"
                          placeholder={alt_query.term_end}
                          onChange={handleChange}
                          value={values.term_end}
                          onBlur={handleBlur}
                        />{' '}
                        {errors.term_end && touched.term_end ? (
                          <div>{errors.term_end}</div>
                        ) : null}
                      </dd>
                    </div>
                    {/* Filing Window */}
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt className="text-base font-medium text-gray-500">
                        Filing Window
                      </dt>
                      <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="date"
                          id="filing_start"
                          name="filing_start"
                          placeholder={alt_query.filing_start}
                          onChange={handleChange}
                          value={values.filing_start}
                          onBlur={handleBlur}
                        />
                        {errors.filing_start && touched.filing_start ? (
                          <div>{errors.filing_start}</div>
                        ) : null}{' '}
                        to
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="date"
                          id="filing_end"
                          name="filing_end"
                          placeholder={alt_query.filing_end}
                          onChange={handleChange}
                          value={values.filing_end}
                          onBlur={handleBlur}
                        />
                        {errors.filing_end && touched.filing_end ? (
                          <div>{errors.filing_end}</div>
                        ) : null}
                      </dd>
                    </div>
                    {/* Contact Info: Email Address */}
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt className="text-base font-medium text-gray-500">
                        Email Address
                      </dt>
                      <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-1">
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="email"
                          id="email_add"
                          name="email_add"
                          placeholder={alt_query.email_add}
                          onChange={handleChange}
                          value={values.email_add}
                          onBlur={handleBlur}
                        />
                        {errors.email_add && touched.email_add ? (
                          <div>{errors.email_add}</div>
                        ) : null}
                      </dd>
                    </div>
                    {/* Contact Info: Facebook */}
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt className="text-base font-medium text-gray-500">
                        Facebook
                      </dt>
                      <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-1">
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="url"
                          id="fbook"
                          name="fbook"
                          placeholder={alt_query.fbook}
                          onChange={handleChange}
                          value={values.fbook}
                          onBlur={handleBlur}
                        />
                        {errors.fbook && touched.fbook ? (
                          <div>{errors.fbook}</div>
                        ) : null}
                      </dd>
                    </div>
                    {/* Contact Info: Instagram */}
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt className="text-base font-medium text-gray-500">
                        Instagram
                      </dt>
                      <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-1">
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="url"
                          id="insta"
                          name="insta"
                          placeholder={alt_query.insta}
                          onChange={handleChange}
                          value={values.insta}
                          onBlur={handleBlur}
                        />
                        {errors.insta && touched.insta ? (
                          <div>{errors.insta}</div>
                        ) : null}
                      </dd>
                    </div>
                    {/* Web Site*/}
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt className="text-base font-medium text-gray-500">
                        Website
                      </dt>
                      <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-1">
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="url"
                          id="website"
                          name="website"
                          placeholder={alt_query.website}
                          onChange={handleChange}
                          value={values.website}
                          onBlur={handleBlur}
                        />
                        {errors.website && touched.website ? (
                          <div>{errors.website}</div>
                        ) : null}
                      </dd>
                    </div>
                    {/* Contact Info: Twitter */}
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt className="text-base font-medium text-gray-500">
                        Twitter
                      </dt>
                      <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-1">
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="url"
                          id="twitter"
                          name="twitter"
                          placeholder={alt_query.twitter}
                          onChange={handleChange}
                          value={values.twitter}
                          onBlur={handleBlur}
                        />
                        {errors.twitter && touched.twitter ? (
                          <div>{errors.twitter}</div>
                        ) : null}
                      </dd>
                    </div>
                    {/* Contact Info: Phone Number */}
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                      <dt className="text-base font-medium text-gray-500">
                        Office Phone Number
                      </dt>

                      <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <input
                          className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                          type="tel"
                          id="phone_num"
                          name="phone_num"
                          placeholder={alt_query.phone_num}
                          onChange={handleChange}
                          value={values.phone_num}
                          onBlur={handleBlur}
                        />
                        {errors.phone_num && touched.phone_num ? (
                          <div>{errors.phone_num}</div>
                        ) : null}
                      </dd>
                    </div>
                    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-1 sm:gap-4 sm:px-6">
                      <h3 className="text-xl leading-6 font-light text-gray-900">
                        Source Data for Verification
                      </h3>
                      <input
                        className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-4 px-8 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        type="url"
                        id="source"
                        defaultValue="Link to Source Material"
                        name="source"
                        placeholder="Enter Source URL"
                        onChange={handleChange}
                        value={values.source}
                        onBlur={handleBlur}
                      />
                      {errors.source && touched.source ? (
                        <div>{errors.source}</div>
                      ) : null}
                      <button
                        type="submit"
                        disabled={isSubmitting}
                        className="bg-indigo-500 w-24 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded-full shadow-sm"
                      >
                        Submit
                      </button>
                    </div>
                  </dl>
                </div>
              </div>
            </form>
          )}
        </Formik>
        )
      </>
    </>
  );
};
