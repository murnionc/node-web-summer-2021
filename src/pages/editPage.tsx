/**
 * File Name: detailedResults
 * Purpose: This file renders the page layout and Correction Temaplte
 *    component
 * Created: July 2021
 * Revised:
 *    > N/A
 * References:
 *    > N/A
 * Author(s): Kevin Carman (OSU 2021)
 **/

import React from 'react';
import { Layout, SEO, CorrectionTemplate } from 'src/containers';

const testPage: React.FC = () => {
  return (
    <>
      {/* Layout component styles the window and displays the NavBar */}
      <Layout>
        <SEO title="Edit details" />
        {/* OfficeHolderTemplate component renders the detailed view of a particular officeholder and handles all data office, office location, and meeting queries */}
        <CorrectionTemplate />
      </Layout>
    </>
  );
};

export default testPage;
